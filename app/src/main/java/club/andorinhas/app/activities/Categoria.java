package club.andorinhas.app.activities;

public class Categoria {

    private String id;
    private String nome;

    public Categoria(String id, String nome) {
        this.id   = id;
        this.nome = nome;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String nome) {
        this.nome = nome;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.nome;
    }
}
