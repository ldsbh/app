package club.andorinhas.app.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import club.andorinhas.app.R;

public class CustomAdapterCategorias extends ArrayAdapter<Categoria> {

    private Context context;

    public CustomAdapterCategorias(Context context, int resource, ArrayList<Categoria> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Categoria categoria = getItem(position);

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.lista_categorias, parent, false);
        }

        TextView txtName = (TextView) view.findViewById(R.id.txt_categoria_nome);
        TextView txtId = (TextView) view.findViewById(R.id.txt_categoria_id);

        txtName.setText(categoria.getName());
        txtId.setText(categoria.getId());

        return view;
    }
}
