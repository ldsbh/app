package club.andorinhas.app.activities;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import club.andorinhas.app.R;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

public class Categorias extends Fragment implements View.OnClickListener {

    private ListView listaCategorias;
    private HttpClient httpClient = HttpClientBuilder.create().build();
    private ArrayList<Categoria> categorias = new ArrayList<Categoria>();
    private CustomAdapterCategorias cCategorias;
    Categoria categoria;
    private String msg = "C Android -> ";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle save) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        View view = inflater.inflate(R.layout.fragment_categorias, container, false);

        listaCategorias = (ListView)view.findViewById(R.id.listaCategorias);

        listaCategorias.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Seu codigo aqui
                Categoria categoria = categorias.get(position);
                Log.d(msg, categoria.getName());

//                Intent intent = new Intent(this, NovaAtividade.class);
//                startActivity(intent);
                
            }

        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Categorias");

        try {
            findCategorias();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    public void findCategorias()throws IOException, JSONException {
        HttpGet clientGet = new HttpGet("http://192.168.25.207:8000/api/auth/categorias");

        clientGet.addHeader("Authorization", "Bearer " + UserSession.getInstance(getContext()).getUserToken());
        clientGet.addHeader("Content-Type", "application/json");
        clientGet.addHeader("Accept", "application/json");

        HttpResponse response = httpClient.execute(clientGet);

        String json = EntityUtils.toString(response.getEntity());

        JSONObject resData = new JSONObject(json);

        for (int i = 0; i < resData.getJSONArray("data").length(); i++) {
            JSONObject data = resData.getJSONArray("data").getJSONObject(i);

            categorias.add(new Categoria(data.getString("id"), data.getString("nome")));
        }

        cCategorias = new CustomAdapterCategorias(getContext(), 0, categorias);
        listaCategorias.setAdapter(cCategorias);
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction ft = this.getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, new Categorias());

        String texto = String.valueOf(view.getId());
        Log.d(null,texto);
    }

    public void updateUI(ArrayList<Categoria> itens) {
        cCategorias.clear();

        if (itens != null) {
            for (Object obj : itens) {
                cCategorias.insert((Categoria) obj, cCategorias.getCount());
            }
        }

        cCategorias.notifyDataSetChanged();
    }
}
