package club.andorinhas.app.models;


import lombok.Data;

@Data
public class Usuarios {
    private Long id;
    private String name;
    private String password;
}
